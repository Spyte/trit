var Discord = require("discord.js")
var config = require(`../config.json`)
module.exports.run = async (client, message, args, embed_color, lang) => {
    let everyone = message.guild.roles.find(`name`, "@everyone");
      await message.channel.overwritePermissions(everyone, {
                    SEND_MESSAGES: true,
                    ADD_REACTIONS: true
               });
      message.reply(lang.commands.unlock.success)
      message.react("🚦")
}
module.exports.help = {
	name: "unlock",
	category:"admin"
}
module.exports.aliases = ["odblokuj"]