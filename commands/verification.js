const Discord = require("discord.js")
const chalk = require("chalk")
const database = require("firebase").database()

module.exports.run = async (client, message, args, embed_color, lang) => {
    if (args[0] == "create") {
        if (!message.member.hasPermission("MANAGE_ROLES")) return message.reply(lang.commands.verification.replies.permission_error)
        let rolesList = []
        for (var i = 1; i <= args.length - 1; i++) {
            let rawRole = args[i]
            rolesList[i - 1] = rawRole.replace("<@&", "").replace(">", "")
        }

        database.ref(`verification/${message.guild.id}`).set({
                channel: message.channel.id,
                rolesList: rolesList
            })
            .then(() => {
                message.reply(lang.commands.verification.replies.success)
                let embed = new Discord.RichEmbed()
                    .setColor(embed_color)
                    .setTitle(lang.commands.verification.embeds.titles[0])
                    .setDescription(lang.commands.verification.emebds.descriptions[0])
                let roles = ""
                for (var i = 1; i <= args.length - 1; i++) {
                    roles += `\n${args[i]}`
                }
                embed.addField(lang.commands.verification.embeds.fields[0], roles)
                message.channel.send(embed)
                message.delete()
                .catch(err => {
                    console.log(chalk.red(`[error] ${err}`))
                })
            })
            .catch(err => {
                message.reply(lang.commands.verification.replies.error + err)
                console.log(chalk.red(`[error] ${err}`))
            })
    } else if (args[0] == "disable") {
        database.ref(`verification/${message.guild.id}`).remove()
        .then(() => {
            message.reply(lang.commands.verification.replies.disable)
        })
        .catch(err => {
            console.log(chalk.red(`[error] ${err}`))
            message.reply(lang.commands.verification.replies.error)
        })
    }
}

module.exports.start = async (client) => {
    client.on("guildMemberAdd", member => {
        database.ref(`verification/${member.guild.id}`).once("value").then(data => {
            database.ref(`settings/${member.guild.id}/prefix`).once("value").then(pf => {
                const prefix = pf.val()

                if (!data.val()) return
                const verification = data.val()

                const filter = msg => msg.channel.id == verification.channel && msg.author.id == member.user.id
                const channel = member.guild.channels.get(verification.channel)
                const collector = new Discord.MessageCollector(channel, filter);
                const rolesToGive = []
                collector.on("collect", msg => {
                    msg.delete()
                        .catch(err => {
                            console.log(chalk.red(`[error] ${err}`))
                        })
                    if (msg.content.includes(`${prefix}verification`)) {
                        rolesToGive[rolesToGive.length] = msg.content.replace(`${prefix}verification `, "")
                    }
                    if (msg.content == prefix + "verification confirm") {
                        const defaultRole = member.guild.roles.get(verification.rolesList[0])
                        member.addRole(defaultRole)
                        try {
                            for (var i = 0; i <= rolesToGive.length - 1; i++) {
                                const role = member.guild.roles.find("name", rolesToGive[i])
                                member.addRole(role)
                                    .catch(err => {
                                        console.log(chalk.red(`[error] ${err}`))
                                    })
                            }
                        } catch (err) {
                            console.log(chalk.red(`[error] ${err}`))
                        }
                        collector.stop()
                    }
                })
            })
        })
    })
}

module.exports.help = {
    name: "verification",
    category: "admin"
}
module.exports.aliases = ["weryfikacja", "weryfikuj"]