const Discord = require("discord.js")
const chalk = require("chalk")
const {
    database
} = require("firebase")

module.exports.run = async (client, message, args, embed_color, lang) => {
    database().ref(`/application/${message.guild.id}`).once("value").then(data => {
        if (!data.val()) return message.reply(lang.commands.apply.no_active)
        const application = data.val()
        const questions = application.questions
        let answers = Array(questions.length)

        var i = 0;

        let embed = new Discord.RichEmbed()
            .setColor(embed_color)
            .setTitle(questions[0])
        message.author.send(embed)
        

    })
}

module.exports.help = {
    name: "apply",
    category: "util"
}

module.exports.aliases = ["aplikuj", "rekrutuj"]