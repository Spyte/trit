const Discord = require("discord.js")
const config = require(`../config.json`)
const google = require("google")
module.exports.run = async (client, message, args, embed_color, lang) => {
    google(args.join(" "), function (err, res){
        let page = 1

        let embed = new Discord.RichEmbed()
        .setTitle(res.links[0].title || "------")
        .setDescription(res.links[0].description|| "-----")
        .setColor(embed_color)
        .setFooter(`1 / ${res.links.length}`)
        .setURL(res.links[0].link)
        message.channel.send(embed).then(msg => {

            msg.react('⏪').then( r => {
            msg.react('⏩')
            const backwardsFilter = (reaction, user) => reaction.emoji.name === '⏪' && user.id === message.author.id;
      const forwardsFilter = (reaction, user) => reaction.emoji.name === '⏩' && user.id === message.author.id;

      const backwards = msg.createReactionCollector(backwardsFilter);
      const forwards = msg.createReactionCollector(forwardsFilter);


      backwards.on('collect', r => {
        r.remove(message.author)
        if (page == 1) return;
        page--;
        embed.setTitle(res.links[page-1].title)
        embed.setDescription(res.links[page-1].description || "-----")
        embed.setURL(res.links[page-1].link)
        embed.setFooter(`${page} / ${res.links.length}`)
        msg.edit(embed)
      })

      forwards.on('collect', r => {
        r.remove(message.author)
        if (page == res.links.length) return;
        page++;
        embed.setTitle(res.links[page-1].title)
        embed.setDescription(res.links[page-1].description || "-----")
        embed.setURL(res.links[page-1].link)
        embed.setFooter(`${page} / ${res.links.length}`)
        msg.edit(embed)
      })

            })})
})
}
module.exports.help = {
	name: "google",
	category:"util"
}
module.exports.aliases = ["wyszukaj"]
