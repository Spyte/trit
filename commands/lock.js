var Discord = require("discord.js")
var config = require(`../config.json`)
const chalk = require("chalk")
module.exports.run = async (client, message, args, embed_color, lang) => {
    let everyone = message.guild.roles.find(`name`, "@everyone");
    await message.channel.overwritePermissions(everyone, {
        SEND_MESSAGES: false,
        ADD_REACTIONS: false
    })
    .then(() => {
        message.reply(lang.commands.lock.replies.done)
        message.react("🚦")
    })
    .catch(err => {
        message.reply(lang.commands.lock.replies.error + " ```" + err + "```")
        console.log(chalk.red("[error] " + err))
    })
}
module.exports.help = {
    name: "lock",
    category: "admin"
}
module.exports.aliases = ["zablokuj"]