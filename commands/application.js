const {database} = require("firebase")
const chalk = require("chalk")

module.exports.run = async(client, message, args, embed_color, lang) => {
    if(!message.member.hasPermission("MANAGE_GUILD")) return message.reply(lang.commands.application.replies.permission_error)
    if(!args[0]) return message.reply(lang.commands.application.replies.use)

    const questions = args.join(" ").split(" | ")
    if(!questions) return message.reply(lang.commands.application.replies.use)

    database().ref(`/application/${message.guild.id}`).set({
        questions: questions
    })
    .then(() => {
        message.reply(lang.commands.application.replies.success)
        message.channel.send(lang.commands.application.replies.instruction)
    })
    .catch(err => {
        console.log(chalk.red(`[error] ${err}`))
        message.reply(lang.commands.application.replies.use)
    })
}

module.exports.help = {
    name: "application",
    category: "admin"
}

module.exports.aliases = ["rekrutacja"]