const Discord = require("discord.js")
const config = require(`../config.json`)
module.exports.run = async (client, message, args, embed_color, lang) => {
    const firebase = require("firebase")
    const database = firebase.database()
    if (!message.member.hasPermission("MANAGE_GUILD")) return message.reply("nie masz permisji :/")
    if (!message.guild.members.get(client.user.id).hasPermission("MANAGE_CHANNELS")) return message.reply("nie mam permisji :/")
    if (args[0] == "start" || !args[0]) {
        message.guild.createChannel("📆", "voice").then(date => {
            message.guild.createChannel("🧙‍", "voice").then(all => {
                message.guild.createChannel("💚", "voice").then(online => {
                    message.guild.createChannel("🦈", "voice").then(highscore => {
                        message.guild.createChannel(`${lang.commands.status.channels[0]} 🐦️`, "voice")
                        message.guild.createChannel(lang.commands.status.channels[1], "voice").then(last => {
                            database.ref(`/status/${message.guild.id}`).set({
                                "date": date.id,
                                "online": online.id,
                                "all": all.id,
                                "last": last.id,
                                "highscore": highscore.id,
                                "enabled": true
                            })
                        })
                    })
                })
            })
        })
    } else if (args[0] == "stop") {
        database.ref(`/status/${message.guild.id}`).set({})
    }
}
module.exports.start = async (client) => {
    const database = require("firebase").database()
    client.on("guildMemberAdd", member => {
        database.ref(`/status/${member.guild.id}`).once("value").then(status => {
            const data = status.val()
            if (!data) return
            if (!data.last) return
            if (data.enabled !== true) return
            client.channels.get(data.last).setName(member.user.tag)
        })
    })
}
module.exports.help = {
    name: "status",
    category: "admin"
}
module.exports.aliases = ["statistics", "statystyki"]