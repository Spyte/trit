const Discord = require("discord.js")
const chalk = require("chalk")

module.exports.run = async(client, message, args, embed_color, lang) => {
    if(!message.member.hasPermission("KICK_MEMBERS")) return message.reply(lang.commands.kick.replies.permission_error)

    const toKick = message.mentions.members.first()
    if(!toKick) return message.reply(lang.commands.kick.replies.no_member)

    const reason = args.join(" ").slice(22)
    if(!reason) return message.reply(lang.commands.kick.replies.no_reason)

    toKick.kick(reason)
    .then(() => {
        let embed = new Discord.RichEmbed()
        .setColor("#ffee00")
        .setAuthor("Kick", toKick.user.displayAvatarURL)
        .setDescription(lang.commands.kick.embeds.descriptions[0] + toKick.user.tag + lang.commands.kick.embeds.descriptions[1] + message.author + lang.commands.kick.embeds.descriptions[2] +"```" + reason + "```")
        .setTimestamp()
        message.channel.send(embed)
    })
    .catch(err => {
        message.reply(lang.commands.kick.replies.cannot_kick)
        console.log(chalk.red(`[error] ${err}`))
    })
}

module.exports.help = {
    name: "kick",
    category: "admin"
}

module.exports.aliases = ["wywal", "wykop"]