const Discord = require("discord.js")
const {
    database
} = require("firebase")

module.exports.run = async (client, message) => {
    const reactions = require("../config/reactions.json")
    const hello = reactions.hello
    const goodNight = reactions.goodNight
    const dinner = reactions.dinner
    const goAway = reactions.goAway
    var chance = Math.floor(Math.random() * 8 - 0) + 0
    if (message.content.toLowerCase().includes("wita")) {
        if (message.author.id == "545637614036844555" && message.guild.id == "615317472781926403") {
            database().ref(`/block`).set({
                block: true
            })
            message.reply("wiesz co? Jebie mnie to, jak ci się nie podoba to super! Już nie musisz się mną martwić - załatwiłeś sobie bloka na wszystkie komendy / funkcje, możesz być z siebie dumny, ponieważ jako jedyny zasłużyłeś sobie coś takiego, tylko potem nie płacz beze mnie :^)")
        }
        if (chance !== 2) return
        var choose = hello[Math.floor(Math.random() * hello.length - 0) + 0]
        message.channel.send(choose)
    }
    if (message.content.toLowerCase().includes("hej")) {
        if (message.author.id == "545637614036844555" && message.guild.id == "615317472781926403") {
            database().ref(`/block`).set({
                block: true
            })
            message.reply("wiesz co? Jebie mnie to, jak ci się nie podoba to super! Już nie musisz się mną martwić - załatwiłeś sobie bloka na wszystkie komendy / funkcje, możesz być z siebie dumny, ponieważ jako jedyny zasłużyłeś sobie coś takiego, tylko potem nie płacz beze mnie :^)")
        }
        if (chance !== 2) return
        var choose = hello[Math.floor(Math.random() * hello.length - 0) + 0]
        message.channel.send(choose)
    }
    if (message.content.toLowerCase().includes("dobranoc")) {
        if (message.author.id == "545637614036844555" && message.guild.id == "615317472781926403") {
            database().ref(`/block`).set({
                block: true
            })
            message.reply("wiesz co? Jebie mnie to, jak ci się nie podoba to super! Już nie musisz się mną martwić - załatwiłeś sobie bloka na wszystkie komendy / funkcje, możesz być z siebie dumny, ponieważ jako jedyny zasłużyłeś sobie coś takiego, tylko potem nie płacz beze mnie :^)")
        }
        if (chance !== 2) return
        var choose = goodNight[Math.floor(Math.random() * goodNight.length - 0) + 0]
        message.channel.send(choose)
    }
    if (message.content.toLowerCase().includes("smacznego")) {
        if (message.author.id == "545637614036844555" && message.guild.id == "615317472781926403") {
            database().ref(`/block`).set({
                block: true
            })
            message.reply("wiesz co? Jebie mnie to, jak ci się nie podoba to super! Już nie musisz się mną martwić - załatwiłeś sobie bloka na wszystkie komendy / funkcje, możesz być z siebie dumny, ponieważ jako jedyny zasłużyłeś sobie coś takiego, tylko potem nie płacz beze mnie :^)")
        }
        if (chance !== 2) return
        var choose = dinner[Math.floor(Math.random() * dinner.length - 0) + 0]
        message.channel.send(choose)
    }
    if (message.content.toLowerCase().includes("ide")) {
        if (message.author.id == "545637614036844555" && message.guild.id == "615317472781926403") {
            database().ref(`/block`).set({
                block: true
            })
            message.reply("wiesz co? Jebie mnie to, jak ci się nie podoba to super! Już nie musisz się mną martwić - załatwiłeś sobie bloka na wszystkie komendy / funkcje, możesz być z siebie dumny, ponieważ jako jedyny zasłużyłeś sobie coś takiego, tylko potem nie płacz beze mnie :^)")
        }

        if (chance !== 2) return
        var choose = goAway[Math.floor(Math.random() * goAway.length - 0) + 0]
        message.channel.send(choose)
    }
    if (message.content.toLowerCase().includes("idę")) {
        if (message.author.id == "545637614036844555" && message.guild.id == "615317472781926403") {
            database().ref(`/block`).set({
                block: true
            })
            message.reply("wiesz co? Jebie mnie to, jak ci się nie podoba to super! Już nie musisz się mną martwić - załatwiłeś sobie bloka na wszystkie komendy / funkcje, możesz być z siebie dumny, ponieważ jako jedyny zasłużyłeś sobie coś takiego, tylko potem nie płacz beze mnie :^)")
        }
        if (chance !== 2) return
        var choose = goAway[Math.floor(Math.random() * goAway.length - 0) + 0]
        message.channel.send(choose)
    }
    if (message.content.toLowerCase().includes("@someone")) {
        if (message.author.id == "545637614036844555" && message.guild.id == "615317472781926403") {
            database().ref(`/block`).set({
                block: true
            })
            message.reply("wiesz co? Jebie mnie to, jak ci się nie podoba to super! Już nie musisz się mną martwić - załatwiłeś sobie bloka na wszystkie komendy / funkcje, możesz być z siebie dumny, ponieważ jako jedyny zasłużyłeś sobie coś takiego, tylko potem nie płacz beze mnie :^)")
        }
        const asciiFaces = ["̿̿ ̿̿ ̿̿ ̿'̿'\̵͇̿̿\З= ( ▀ ͜͞ʖ▀) =Ε/̵͇̿̿/’̿’̿ ̿ ̿̿ ̿̿ ̿̿   ", "ʕ•ᴥ•ʔ", "༼ つ ◕_◕ ༽つ", "(◕‿◕✿)", "༼ つ ͡° ͜ʖ ͡° ༽つ", "(╯°□°）╯︵ ┻━┻"]
        let members = []
        let i = 0
        message.guild.members.forEach(member => {
            members[i] = member.user.id
            i++
        });
        message.channel.send("<@" + members[Math.floor(Math.random() * members.length)] + "> " + asciiFaces[Math.floor(Math.random() * asciiFaces.length)])
    }
    if (message.content.toLowerCase().includes("xd")) {
        if (message.author.id == "545637614036844555" && message.guild.id == "615317472781926403") {
            database().ref(`/block`).set({
                block: true
            })
            message.reply("wiesz co? Jebie mnie to, jak ci się nie podoba to super! Już nie musisz się mną martwić - załatwiłeś sobie bloka na wszystkie komendy / funkcje, możesz być z siebie dumny, ponieważ jako jedyny zasłużyłeś sobie coś takiego, tylko potem nie płacz beze mnie :^)")
        }
        var chanceXD = Math.floor(Math.random() * 100 - 0) + 0
        if (chanceXD !== 2) return
        var choose = "xD"
        message.channel.send(choose)
    }

}