const Discord = require("discord.js")
const chalk = require("chalk")

module.exports.run = async (client) => {
    const {
        database
    } = require("firebase")

    client.on("message", message => {
        if (!message.guild) return;
        if (message.author.id == client.user.id) return;

        database().ref(`/anon/${message.guild.id}/channel`).once("value").then(data => {
            const channelId = data.val()
            if (!channelId) return;
            if (message.channel.id != channelId) return;

            const {
                database
            } = require("firebase")


            database().ref(`/settings/${message.guild.id}`).once("value").then(data => {
                const settings = data.val()
                const channel = client.channels.get(channelId)

                const commandName = [`${settings.prefix}anon`, `${settings.prefix}anonimowy`]

                if (commandName.includes(message.content)) return;

                let lang
                const embed_color = settings.embed_color

                if (settings.language == "PL") lang = require("../languages/pl.json")
                else lang = require("../languages/en.json")


                let embed = new Discord.RichEmbed()
                    .setColor(embed_color)
                    .setAuthor(lang.commands.anon.embeds.titles[0])
                    .setDescription(message.content)
                    .setTimestamp()

                message.delete()
                    .then(() => {
                        channel.send(embed).then(msg => {})
                    })
                    .catch(err => {
                        message.guild.owner.send(lang.commands.anon.replies.noPermissions + `<#${channel.id}>`)
                        console.log(chalk.red(`[error] ${err}`))
                    })
            })
        })
    })
}