require("dotenv").config()
const chalk = require('chalk');
const log = console.log
const Discord = require("discord.js")
let client = new Discord.Client({
  disableEveryone: true
})
const config = require("./config.json")

let block = false

// client.setMaxListeners(20)

client.commands = new Discord.Collection()
client.owner = "Spyte"

var fs = require("fs")
var reactions = require('./listeners/reaction.js')
const moment = require("moment")

var firebase = require('firebase')
var fireconfig = {
  apiKey: process.env.API,
  authDomain: process.env.ID + ".firebaseapp.com",
  databaseURL: `https://${process.env.ID}.firebaseio.com`,
  projectId: process.env.ID,
  storageBucket: process.env.ID + ".appspot.com",
  messagingSenderId: process.env.SENDER
};

firebase.initializeApp(fireconfig);
var database = firebase.database();

fs.readdir(`./commands/`, async (err, files) => {
  if (err) console.log(err)
  let jsfile = files.filter(f => f.split(".").pop() == "js")
  if (jsfile.length <= 0) {
    console.log("Nie znaleziono komend!")
  }
  jsfile.forEach((f, i) => {
    let props = require(`./commands/${f}`)
    console.log(chalk.cyan(`[Załadowano] ${f}`))
    client.commands.set(props.help.name, props)
  })

})

client.setMaxListeners(30)

const antiRaid = require("./listeners/anti-raid.js")
const cleverbot = require("./listeners/cleverbot.js")
const pool = require("./listeners/pool.js")
const logs = require("./listeners/logs.js")
const dankmemes = require("./commands/dankmemes.js")
const status = require("./commands/status.js")
const wordsFilter = require("./commands/wordsfilter.js")
const counting = require("./commands/counting.js")
const verification = require("./commands/verification.js")
const xp = require("./listeners/xp.js")
const propositions = require("./listeners/propositions.js")
const anon = require("./listeners/anon.js")
const sierociniec = require("./listeners/sierociniec.js")

cleverbot.run(client).then(() => console.log(chalk.blue("[listener] cleverbot.js")))
pool.run(client).then(() => console.log(chalk.blue("[listener] pool.js")))
logs.run(client).then(() => console.log(chalk.blue("[listener] logs.js")))
dankmemes.start(client, "ff4500").then(() => console.log(chalk.blue("[listener] dankmemes.js")))
status.start(client).then(() => console.log(chalk.blue("[listener] status.js")))
wordsFilter.start(client).then(() => console.log(chalk.blue("[listener] wordsFilter.js")))
counting.start(client).then(() => console.log(chalk.blue("[listener] counting.js")))
verification.start(client).then(() => console.log(chalk.blue("[listener] verification.js")))
xp.start(client).then(() => console.log(chalk.blue(`[listener] xp.js`)))
propositions.run(client).then(() => console.log(chalk.blue(`[listener] propositions.js`)))
anon.run(client).then(() => console.log(chalk.blue(`[listener] anon.js`)))
sierociniec.run(client).then(() => console.log(chalk.blue(`[listener] sierociniec.js`)))

client.on("ready", () => {
  var weather = require("weather-js")
  var day = ""
  const moment = require("moment")
  setInterval(function () {
    switch (new Date().getDay()) {
      case 0:
        day = "Sunday";
        break;
      case 1:
        day = "Monday";
        break;
      case 2:
        day = "Tuesday";
        break;
      case 3:
        day = "Wednesday";
        break;
      case 4:
        day = "Thursday";
        break;
      case 5:
        day = "Friday";
        break;
      case 6:
        day = "Saturday";
    }
    weather.find({
      search: "warsaw",
      degreeType: 'C'
    }, function (err, currentult) {
      if (err) return console.log(chalk.red("[error] Nie można odczytać pogody, rozłączono z serwerem"))
      var current = currentult[0].current
      var today = moment(new Date()).format("DD.MM")
      var games = ["$help", "I'm serving " + client.guilds.size + " guilds", "Serving " + client.users.size + " users", "Today is " + day + " " + today, "Temperature: " + current.temperature + "°C"]
      var choose = Math.floor(Math.random() * games.length - 0) + 0
      client.user.setActivity(games[choose])
    })
  }, 12000)

  console.log(`[client] Zalogowano jako ${client.user.tag}`)
  console.log(`[client] Obsługa ${client.guilds.size} serwerów / ${client.users.size} osób`)
  // client.commands.forEach((command)=> {
  //   if(!command.help.description) return
  //   database.ref(`/commands/${command.help.category}/${command.help.name}`).set({
  //     "name":command.help.name,
  //     "use":command.help.use,
  //     "description":command.help.description
  //   })
  // })
})

client.on("ready", () => {
  setInterval(function () {
    client.guilds.forEach(async (guild, id) => {
      database.ref(`/status/${id}`).once("value").then(status => {
        const data = status.val()
        database.ref(`/settings/${guild.id}/language`).once("value").then(l => {
          const language = l.val()

          function getHighscore(online, d, lang) {
            let score

            database.ref(`highscore/${id}/score`).once("value").then(data => {
              if (!data.val()) {
                database.ref(`highscore/${id}/`).set({
                  score: online
                })
                score = online
              } else {
                score = data.val()
              }
              if (score < online) {
                database.ref(`highscore/${id}/`).set({
                  score: online
                })
                score = online
              }
              if (d.highscore) guild.channels.get(d.highscore).setName(`${lang.commands.status.channels[4]}: ${score.toString()}`)
              // console.log(score)
            })
          }

          let lang
          if (language == "PL") {
            lang = require("./languages/pl.json")
          } else if (language == "EN") {
            lang = require("./languages/en.json")
          } else {
            lang = require("./languages/en.json")
          }
          if (!data) return
          if (data.enabled !== true) return
          if (!guild.channels.get(data.date)) database.ref(`/status/${id}`).set({
            "enabled": false
          })
          if (!guild.channels.get(data.online)) database.ref(`/status/${id}`).set({
            "enabled": false
          })
          if (!guild.channels.get(data.all)) database.ref(`/status/${id}`).set({
            "enabled": false
          })
          if (!data.date) return
          let online = guild.members.filter(member => member.user.presence.status !== 'offline');
          guild.channels.get(data.date).setName(`${lang.commands.status.channels[2]}: ${moment.utc(new Date()).format("DD.MM.YYYY")}`).catch(err => {
            console.log("[error] Błąd przy ustawianiu nazwy")
          })
          guild.channels.get(data.online).setName(`Online: ${online.size}`)
          guild.channels.get(data.all).setName(`${lang.commands.status.channels[3]}: ${guild.members.size}`)


          getHighscore(online.size, data, lang)

        })
      })
    })
  }, 10000)
})

// client.on('debug', m => console.log(chalk.yellow("[debug] " + m)));
// client.on('warn', m => console.log(chalk.orange("[warn] " + m)));
// client.on('error', m => console.log(chalk.red("[error] " + m)));

// process.on('uncaughtException', error => chalk.redBright("[error] " + error))

client.on("message", message => {
  database.ref(`/block/block`).once("value").then(d => {
    if (d.val() == true) block = true

    if (block == true && message.author.id == "545637614036844555") return;
    if (message.channel.type == "dm") return

    database.ref(`/commands/${message.guild.id}/switches`).once("value").then(sch => {
      database.ref(`/settings/${message.guild.id}`).once("value").then(data => {
        database.ref(`/settings/${message.guild.id}/language`).once("value").then(l => {
          const settings = data.val()
          if (message.author.bot) return

          if (message.channel.type == "dm") return
          antiRaid.run(message, client)
          database.ref(`/settings/${message.guild.id}/version`).once("value")
            .then(version => {
              if (version.val() !== 5) {
                database.ref(`/settings/${message.guild.id}`).set({
                  "version": 5,
                  "embed_color": "#1be5e2",
                  "prefix": "$",
                  "wmsg": "Witaj na serwerze /member/",
                  "wlcm": false,
                  "wchan": null,
                  "wrole": "@everyone",
                  "gbmsg": "papa",
                  "gbay": false,
                  "leveling": true,
                  "logs": false,
                  "logschan": null,
                  "language": "EN"
                })
                message.channel.send("New config is generated, to change language type: $language EN/PL")
              }
              database.ref(`commands/${message.guild.id}/switches/controlPoint`).once("value").then(point => {
                if (point.val() != client.commands.size) {
                  let commandsArray = Array(0)
                  client.commands.forEach(cmd => {
                    let cmdName = cmd.help.name
                    commandsArray[commandsArray.length] = cmdName
                  })

                  let JSONstring = ""

                  for (var i = 0; i <= commandsArray.length - 1; i++) {
                    JSONstring += ', "' + commandsArray[i] + '": true'
                  }

                  JSONstring = `{"controlPoint": ${client.commands.size}, ${JSONstring.replace(", ", "")}}`
                  const switches = JSON.parse(JSONstring)

                  database.ref(`commands/${message.guild.id}`).set({
                    switches
                  }).catch(err => {
                    console.log(chalk.red(`[error] ${err}`))
                  })
                }
              })
              if (l.val() == "PL") reactions.run(client, message)

              var embed_color = settings.embed_color
              let prefix = settings.prefix

              if (!message.content.startsWith(prefix)) return

              let messageArray = message.content.split(" ")
              let cmd = messageArray[0]
              var args = message.content.slice(prefix.length).trim().split(/ +/g)
              var command = args.shift().toLowerCase()
              const commandName = cmd.slice(prefix.length)

              let commandfile = client.commands.get(cmd.slice(prefix.length)) ||
                client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
              let lang
              if (l.val() == "PL") {
                lang = require("./languages/pl.json")
              } else {
                lang = require("./languages/en.json")
              }
              let commandEnabled
              const switches = sch.val()

              commandEnabled = switches[commandName]

              if (commandfile && commandEnabled) commandfile.run(client, message, args, embed_color, lang)
              else if (commandEnabled == false) {
                message.reply("<:disable:665227998324326441> " + lang.commands.command.replies.disabled)
              }
            })
        })
      })
    })
  })
})

var Music = require('discord.js-musicbot-addon-v2');
var music = new Music(client, {
  prefix: config.prefix,
  youtubeKey: process.env.API,
  embedColor: 1828322,
  enableQueueStat: true,
  botAdmins: [367390191721381890],
  clearOnLeave: true,
  disableVolume: true,
  djRole: "@everyone"
});

client.on('guildMemberRemove', async member => {
  database.ref(`/settings/${member.guild.id}/gbay`).once('value')
    .then(on => {
      if (on.val() == false) return
      database.ref(`/settings/${member.guild.id}/gbmsg`).once('value')
        .then(msg => {
          database.ref(`/settings/${member.guild.id}/gbchan`).once('value')
            .then(chan => {
              var wlcm = msg.val()
              if (!msg.val()) return;
              if (!chan.val()) return;
              client.channels.get(chan.val()).send(wlcm.replace("/member/", "**" + member.user.tag + "**"))
                .catch(err => chalk.red(`[error] ${err}`))
            })
        })
    })
})


var on2 = database.ref('/admin/eval');
on2.on('value', function (result) {
  if (result.val() == "everything done") return;
  eval(`async function go() {
      ${result.val()}
    }
    go()`)
  database.ref('/admin/').set({
    "eval": "everything done"
  });
});
client.login(process.env.TOKEN)